import React, { Component } from 'react'
import {
  View,
  Image,
} from 'react-native';
import {
  Container,
  Header,
  Left,
  Right,
  Body,
  Title,
  Content,
  Item,
  Button,
  Picker,
  Card,
  CardItem,
  Text,
} from 'native-base';
import {
  MKSlider,
  MKButton,
} from 'react-native-material-kit';
import MapView from 'react-native-maps';
import { Actions } from 'react-native-router-flux';
import uuidV4 from 'uuid/v4';
import update from 'immutability-helper';
import PopupDialog from 'react-native-popup-dialog';
import axios from 'axios';


const busMapMarkerSource = require('../../assets/img/busMapMarker.png');
const locationMapMarkerSource =
  require('../../assets/img/locationMapMarker.png');
const api = 'https://busina-164603.appspot.com/api';

// ============================================================================
// Test shizzz
// ============================================================================


const mockNearbyBuses = [
  {
    distance: '3.5 km',
    plate_no: 'LOL-1234',
    seat_count: 150,
    operator: 'Earth Star (Mock)',
    passenger_count: 23,
    location: 'Inang Bayan, Quezon Ave, Diliman, Quezon City, ' +
        'Metor Manila, Philippines',
    duration: '10 mins',
  },
  {
    distance: '4.2 km',
    plate_no: 'ZJM-11342',
    seat_count: 23,
    operator: 'Kellen Liner (Mock)',
    passenger_count: 12,
    location: 'San Jose del Monte Bulacan',
    duration: '1 hr',
  },
  {
    distance: '0.9 km',
    plate_no: 'JIG-11342',
    seat_count: 70,
    operator: 'Jerryliner (Mock)',
    passenger_count: 45,
    location: 'Pasay',
    duration: '12 hrs',
  },
];

function randInt(a, b) {
  return (a + Math.round(Math.random() * 1e12)) % (b - a);
}

function patchBusCoords(buses, latitude, longitude) {
  return buses.map((bus) => {
    const newBus = { ...bus };
    const degree = randInt(0, 360) * (Math.PI / 180);
    const distance = randInt(0, 7) * (1 / 1110);

    newBus.coordinate = {
      latitude: latitude + (distance * Math.cos(degree)),
      longitude: longitude + (distance * Math.sin(degree)),
    };
    return newBus;
  });
}

// end >> Test shizz
// ----------------------------------------------------------------------------

function processRoutes(responseRoutes) {
  return responseRoutes.map((route) => {
    const splitted = route.split(' - ');
    return {
      key: uuidV4(),
      from: splitted[0],
      to: splitted[1],
      flip: false,
      value: route,
    };
  });
}

class ValueText extends Component {
  state = {
    curValue: this.props.initial,
  }

  setValue(curValue) {
    this.setState({ curValue });
  }

  render() {
    return (
      <Text>{this.state.curValue} km</Text>
    );
  }
}

function computeLoadLevel(onBoard, capacity) {
  const loadPercent = onBoard / (capacity / 2.5) * 100;
  if (loadPercent <= 33) {
    return 'Light';
  } else if (loadPercent <= 66) {
    return 'Medium';
  } else {
    return 'High';
  }
}

export default class RequestPage extends Component {

  state = {
    coordinate: {
      latitude: 0.0,
      longitude: 0.0,
    },
    region: {
      latitude: 0.0,
      longitude: 0.0,
      latitudeDelta: 0.015,
      longitudeDelta: 0.0121,
    },
    radius: 0.3,
    routes: [],
    nearbyBuses: [],
    selectedRoute: undefined,
    flipped: false,
  }

  componentDidMount = () => {
    this.requestCurrentLocation();
    this.requestRoutes();
  }

  requestCurrentLocation = async () => {
    navigator.geolocation.getCurrentPosition(
      this.onGetPositionSuccess,
      this.onGetPositionFailure,
      { enableHighAccuracy: false, timeout: 20000, maximumAge: 1000 },
    );
  }

  onGetPositionSuccess = ({ coords: { latitude, longitude } }) => {

    this.setState({
      region: {
        ...this.state.region,
        latitude,
        longitude,
      },
      coordinate: {
        latitude,
        longitude,
      },
    });
  }

  onGetPositionFailure = (error) => {
    console.log(error);
  }

  onMarkerDragEnd = ({ nativeEvent: { coordinate } }) => {
    this.setState({ coordinate });
  }

  onMapViewRegionChange = (region) => {
    this.setState({ region });
  }

  onPickerValueChange = (selectedRoute) => {
    if (this.state.flipped) {
      this.setState({ flipped: !this.state.flipped });
      return;
    }
    this.setState({ selectedRoute });
  }

  onFlipButtonPress = () => {
    const selectedRoute = this.state.routes.find(route =>
      route.key === this.state.selectedRoute,
    );
    const index = this.state.routes.indexOf(selectedRoute);


    const newRoutes = update(this.state.routes, {
      [index]: {
        $apply: (route) => {
          const newRoute = { ...route };
          const temp = newRoute.from;
          newRoute.from = newRoute.to;
          newRoute.to = temp;
          newRoute.flip = !newRoute.flip;
          return newRoute;
        },
      },
    });

    this.setState({
      routes: newRoutes,
      flipped: !this.state.flipped,
    });
  }

  onGetBusesPress = () => {
    this.requestNearbyBuses();
  }

  requestNearbyBuses = () => {
    const routeItem = this.state.routes.find(r => (
      r.key === this.state.selectedRoute
    ));

    const jsonBody = {
      route: `${routeItem.value}`,
      flip: routeItem.flip,
      ...this.state.coordinate,
      radius: this.state.radius,
      mode: 'json',
    };

    axios.get(`${api}/buses`, { params: jsonBody  })
      .then((responseNearbyBuses) => {
        const buses = responseNearbyBuses.data.nearby_buses.map(bus => ({
          ...bus,
          coordinate: {
            latitude: bus.latitude,
            longitude: bus.longitude,
          },
        }));

        const otherBuses = patchBusCoords(
          mockNearbyBuses,
          this.state.coordinate.latitude,
          this.state.coordinate.longitude,
        );
        const newState = update(this.state, {
          $set: { nearbyBuses: [ ...buses, ...otherBuses ] },
        });
        this.setState(newState);
      });
  }

  requestRoutes = () => {
    // setTimeout(() => {
    //   const responseRoutes = processRoutes(mockRoutes);
    //   this.setState({
    //     routes: responseRoutes,
    //     selectedRoute: responseRoutes[0].key,
    //   });
    // }, 500);

    // return;
    // API Call start
    axios.get(`${api}/routes`)
      .then((response) => {
        const responseRoutes = processRoutes(response.data.routes);
        this.setState({
          routes: responseRoutes,
          selectedRoute: responseRoutes && responseRoutes[0].key,
        });
      });
  }


  render() {
    return (
      <Container>
        <Header>
          <Body>
            <Title>BusInA</Title>
          </Body>
        </Header>
        <Content style={{ padding: 5 }}>

          <Card>
            <CardItem header>
              <Left>
                <Text>Location</Text>
              </Left>
              <Right>
                <Button
                  small
                  onPress={this.requestCurrentLocation}
                >
                  <Text>Reload</Text>
                </Button>
              </Right>
            </CardItem>
            <MapView
              style={{ height: 300 }}
              initialRegion={this.state.region}
              region={this.state.region}
              onRegionChange={this.onMapViewRegionChange}
            >
              <MapView.Marker
                coordinate={this.state.coordinate}
                onDragEnd={this.onMarkerDragEnd}
                image={locationMapMarkerSource}
                draggable
              />
              {this.state.nearbyBuses.map(bus => {
                const loadLevel = computeLoadLevel(
                  bus.passenger_count,
                  bus.seat_count,
                );

                return (
                  <MapView.Marker
                    key={bus.plate_no}
                    coordinate={bus.coordinate}
                    title={`${bus.operator} (${bus.plate_no})`}
                    description={`${loadLevel} load - ${bus.distance} far`}
                    image={busMapMarkerSource}
                  />
                );
              })}
              <MapView.Circle
                center={this.state.coordinate}
                radius={this.state.radius * 1000 / 2}
              />
            </MapView>
          </Card>

          <Card>
            <CardItem header>
              <Left>
                <Text>Routes</Text>
              </Left>
              <Right>
                <Button
                  small
                  onPress={this.onFlipButtonPress}
                >
                  <Text>flip</Text>
                </Button>
              </Right>
            </CardItem>
            <Picker
              supportedOrientation={['portrait', 'landscape']}
              iosHeader="Select Route"
              selectedValue={this.state.selectedRoute}
              onValueChange={this.onPickerValueChange}
              mode="dropdown"
              style={{
                marginLeft: 10,
                marginRight: 10,
              }}
            >
              {this.state.routes.map(route => (
                <Item
                  key={route.key}
                  label={`${route.from} - ${route.to}`}
                  value={route.key}
                />
              ))}
            </Picker>
          </Card>

          <Card>
            <CardItem header>
              <Left>
                <Text>Radius</Text>
              </Left>
              <Right>
                <ValueText
                  ref={(valueText) => { this.radiusValueLabel = valueText; }}
                  initial={this.state.radius}
                />
              </Right>
            </CardItem>
            <MKSlider
              min={0.1}
              max={10}
              step={0.01}
              value={this.state.radius}
              onChange={(value) => {
                this.radiusValueLabel.setValue(value.toFixed(2));
              }}
              onConfirm={(radius) => {
                this.setState({ radius });
              }}
            />
          </Card>

          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'center',
              margin: 10,
            }}
          >
            <Button rounded onPress={this.onGetBusesPress}>
              <Text>Get buses</Text>
            </Button>
          </View>

          <View style={{ height: 10 }} />

        </Content>
      </Container>
    );
  }
}
