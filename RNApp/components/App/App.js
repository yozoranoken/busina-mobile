import React from 'react';
import { Router, Scene } from 'react-native-router-flux';

import RequestPage from '../RequestPage';


const App = () => (
  <Router hideNavBar>
    <Scene key="root">
      <Scene
        key="requestPage"
        component={RequestPage}
        initial
      />
    </Scene>
  </Router>
);

export default App;
