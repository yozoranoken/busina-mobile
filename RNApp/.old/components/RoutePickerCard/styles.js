import { StyleSheet } from 'react-native';

const colors = {
  WHITE: '#ffffff',
  GRAY: '#343434',
  ORANGE: '#DB4C2C',
};

export const rawStyles = {
  card: {
    borderRadius: 5,
  },
  cardHeaderTitle: {
    color: colors.ORANGE,
  },
  cardPicker: {
    margin: 10,
  },
  cardPickerItem: {
    color: colors.GRAY,
  },
  flipButton: {
    backgroundColor: colors.ORANGE,
  },
};

export default StyleSheet.create(rawStyles);
