import React, { Component } from 'react';

import {
  Label,
  Text,
  Button,
  Picker,
  Item,
  Card,
  CardItem,
  Left,
  Right,
  connectStyle,
} from 'native-base';

import { rawStyles } from './styles';


@connectStyle('BusInA.RoutePickerCard', rawStyles)
// eslint-disable-next-line react/prefer-stateless-function
class RoutePickerCard extends Component {
  render() {
    const {
      onValueChange,
      onFlip,
      selectedValue,
      routes,
      flipped,
    } = this.props;
    const nbStyles = this.props.style;

    return (
      <Card style={nbStyles.card}>
        <CardItem header>
          <Left>
            <Label style={nbStyles.cardHeaderTitle}>Route</Label>
          </Left>
          <Right>
            <Button rounded small style={nbStyles.flipButton}>
              <Text>flip</Text>
            </Button>
          </Right>
        </CardItem>
        <Picker
          style={nbStyles.cardPicker}
          prompt="Select a route"
          mode="dropdown"
          selectedValue={selectedValue}
          onValueChange={onValueChange}
        >
          {routes.map(route => (
            <Item
              style={nbStyles.cardPickerItem}
              key={route.route}
              label={route.route}
              value={route.route}
            />
          ))}
        </Picker>
      </Card>
    );
  }
}

export default RoutePickerCard;
