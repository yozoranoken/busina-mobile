import React, { Component } from 'react';
import { Slider } from 'react-native';
import {
  Container,
  Header,
  Body,
  Content,
  Title,
  Button,
  connectStyle,

  Card,
  CardItem,
  Label,
  Text,
} from 'native-base';
import { rawStyles } from './styles';
import RoutePickerCard from '../RoutePickerCard';


@connectStyle('BusInA.ConfigPage', rawStyles)
class ConfigPage extends Component {
  state = {
    selectedRoute: null,
    flipped: false,
    radius: 1,
    routes: [
      { route: 'Fairview - Quiapo' },
      { route: 'San Jose del Monte - NAIA' },
      { route: 'Cubao - NAIA' },
      { route: 'EDSA - Pasay' },
      { route: 'Taytay - Cubao' },
    ],
  }

  onRoutePickerValueChange = (route) => {
    this.setState({ selectedRoute: route });
  }

  onRadiusSliderValueChange = (value) => {
    this.setState({ radius: value });
  }

  onFlip = (value) => {
    this.setState({ flipped: value });
  }

  render() {
    const { selectedRoute, routes } = this.state;
    const nbStyles = this.props.style;

    return (
      <Container>
        <Header style={nbStyles.header}>
          <Body>
            <Title style={nbStyles.headerTitle}>BusInA</Title>
          </Body>
        </Header>
        <Content padder>
          <RoutePickerCard
            routes={routes}
            selectedValue={selectedRoute || routes[0]}
            onValueChange={this.onRoutePickerValueChange}
            onFlip={this.onFlip}
            flipped={this.state.flipped}
          />
          <Card style={{ borderRadius: 5 }}>
            <CardItem header>
              <Label>Radius</Label>
            </CardItem>
            <Slider
              style={{
                marginLeft: 5,
                marginRight: 5,
                marginTop: 15,
                marginBottom: 15,
              }}
              minimumTrackTintColor={'#DB4C2C'}
              maximumTrackTintColor={'#DB4C2C'}
              thumbTintColor={'#DB4C2C'}
            />
          </Card>
          <Card style={{ borderRadius: 5 }}>
            <Button
              block
              style={{
                backgroundColor: '#DB4C2C',
                borderRadius: 5,
              }}
            >
              <Text>Get Buses</Text>
            </Button>
          </Card>
        </Content>
      </Container>
    );
  }
}

export default ConfigPage;
