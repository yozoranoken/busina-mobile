import { StyleSheet } from 'react-native';

const colors = {
  WHITE: '#ffffff',
  GRAY: '#343434',
  ORANGE: '#DB4C2C',

}

export const rawStyles = {
  header: {
    backgroundColor: colors.WHITE,
  },
  headerTitle: {
    color: colors.GRAY,
  },
};

export default StyleSheet.create(rawStyles);
