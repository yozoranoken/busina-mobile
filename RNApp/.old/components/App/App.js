import React from 'react';
import { View } from 'react-native';

import ConfigPage from '../ConfigPage';
import BusesPage from '../BusesPage';


const App = () => (
  // <ConfigPage />
  <BusesPage />
);

export default App;
